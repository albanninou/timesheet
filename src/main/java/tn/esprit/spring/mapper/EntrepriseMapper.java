package tn.esprit.spring.mapper;

import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.dto.EntrepriseDTO;

public class EntrepriseMapper {

    private EntrepriseMapper() {
    }

    public static Entreprise toEntity(EntrepriseDTO dto) {
        return new Entreprise().setId(dto.getId())
                               .setName(dto.getName())
                               .setRaisonSocial(dto.getRaisonSocial())
                               .setDepartements(dto.getDepartements());
    }
}
