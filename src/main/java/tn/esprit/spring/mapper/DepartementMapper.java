package tn.esprit.spring.mapper;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.dto.DepartementDTO;

public class DepartementMapper {

    private DepartementMapper() {
    }

    public static Departement toEntity(DepartementDTO departementdto) {
        return new Departement().setId(departementdto.getId())
                                .setName(departementdto.getName())
                                .setEmployes(departementdto.getEmployes())
                                .setMissions(departementdto.getMissions())
                                .setEntreprise(departementdto.getEntreprise());
    }
}
