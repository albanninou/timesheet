package tn.esprit.spring.mapper;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.dto.ContratDTO;

public class ContratMapper {

    private ContratMapper() {

    }

    public static Contrat toEntity(ContratDTO contratDTO) {
        return new Contrat().setReference(contratDTO.getReference())
                            .setTypeContrat(contratDTO.getTypeContrat())
                            .setSalaire(contratDTO.getSalaire())
                            .setEmploye(contratDTO.getEmploye())
                            .setDateDebut(contratDTO.getDateDebut());
    }
}
