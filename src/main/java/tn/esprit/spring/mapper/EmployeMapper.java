package tn.esprit.spring.mapper;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.dto.EmployeDTO;

public final class EmployeMapper {

    private EmployeMapper() {
    }

    public static Employe toEntity(EmployeDTO dto) {
        return new Employe().setId(dto.getId())
                            .setEmail(dto.getEmail())
                            .setPrenom(dto.getPrenom())
                            .setNom(dto.getNom())
                            .setActif(dto.isActif())
                            .setContrat(dto.getContrat())
                            .setDepartements(dto.getDepartements())
                            .setPassword(dto.getPassword())
                            .setTimesheets(dto.getTimesheets())
                            .setRole(dto.getRole());
    }
}
