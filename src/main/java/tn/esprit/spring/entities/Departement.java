package tn.esprit.spring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
public class Departement implements Serializable {

    private static final long serialVersionUID = -357738161698377833L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    //@JsonManagedReference
    @JsonIgnore
    @ManyToMany
    private List<Employe> employes;

    @OneToMany(mappedBy = "departement")
    private List<Mission> missions;

    @ManyToOne
    private Entreprise entreprise;

    public Departement() {
        super();
    }

    public Departement(String name) {
        this.name = name;
    }

    public Departement(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Departement setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Departement setName(String name) {
        this.name = name;
        return this;
    }

    public List<Employe> getEmployes() {
        return employes;
    }

    public Departement setEmployes(List<Employe> employes) {
        this.employes = employes;
        return this;
    }

    public List<Mission> getMissions() {
        return missions;
    }

    public Departement setMissions(List<Mission> missions) {
        this.missions = missions;
        return this;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public Departement setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
        return this;
    }

    @Override
    public String toString() {
        return "Departement [id=" + id + ", name=" + name + "]";
    }
}
