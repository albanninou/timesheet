package tn.esprit.spring.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Contrat implements Serializable {

    private static final long serialVersionUID = 6191889143079517027L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int reference;

    @Temporal(TemporalType.DATE)
    private Date dateDebut;

    private String typeContrat;


    @OneToOne
    private Employe employe;

    private float salaire;

    public Contrat() {
        super();
    }

    public Contrat(Date dateDebut, String typeContrat, float salaire) {
        this.dateDebut = dateDebut;
        this.typeContrat = typeContrat;
        this.salaire = salaire;
    }

    public int getReference() {
        return reference;
    }

    public Contrat setReference(int reference) {
        this.reference = reference;
        return this;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Contrat setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public String getTypeContrat() {
        return typeContrat;
    }

    public Contrat setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
        return this;
    }

    public Employe getEmploye() {
        return employe;
    }

    public Contrat setEmploye(Employe employe) {
        this.employe = employe;
        return this;
    }

    public float getSalaire() {
        return salaire;
    }

    public Contrat setSalaire(float salaire) {
        this.salaire = salaire;
        return this;
    }
}
