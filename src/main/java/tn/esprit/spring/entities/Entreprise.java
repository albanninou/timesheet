package tn.esprit.spring.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Entreprise implements Serializable {

    private static final long serialVersionUID = 3152690779535828408L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String raisonSocial;

    @OneToMany(mappedBy = "entreprise",
               cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
               fetch = FetchType.EAGER)
    private List<Departement> departements = new ArrayList<>();

    public Entreprise() {
        super();
    }

    public Entreprise(String name, String raisonSocial) {
        this.name = name;
        this.raisonSocial = raisonSocial;
    }

    public int getId() {
        return id;
    }

    public Entreprise setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Entreprise setName(String name) {
        this.name = name;
        return this;
    }

    public String getRaisonSocial() {
        return raisonSocial;
    }

    public Entreprise setRaisonSocial(String raisonSocial) {
        this.raisonSocial = raisonSocial;
        return this;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    public Entreprise setDepartements(List<Departement> departements) {
        this.departements = departements;
        return this;
    }


    public void addDepartement(Departement departement) {
        departement.setEntreprise(this);
        this.departements.add(departement);
    }
}
