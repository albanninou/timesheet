package tn.esprit.spring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
public class Employe implements Serializable {

    private static final long serialVersionUID = -1396669830860400871L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String prenom;

    private String nom;

    //@Column(unique=true)
    //@Pattern(regex=".+\@.+\..+")
    private String email;

    private String password;

    private boolean actif;

    @Enumerated(EnumType.STRING)
    //@NotNull
    private Role role;

    //@JsonBackReference
    @JsonIgnore
    @ManyToMany(mappedBy = "employes", fetch = FetchType.EAGER)
    //@NotNull
    private List<Departement> departements;

    @JsonIgnore
    //@JsonBackReference
    @OneToOne(mappedBy = "employe")
    private Contrat contrat;

    @JsonIgnore
    //@JsonBackReference
    @OneToMany(mappedBy = "employe")
    private List<Timesheet> timesheets;

    public Employe() {
    }

    public Employe(int id, String prenom, String nom, String email, String password, boolean actif, Role role) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.password = password;
        this.actif = actif;
        this.role = role;
    }


    public Employe(String nom, String prenom, String email, String password, boolean actif, Role role) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.password = password;
        this.actif = actif;
        this.role = role;
    }

    public Employe(String nom, String prenom, String email, boolean actif, Role role) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.actif = actif;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public Employe setId(int id) {
        this.id = id;
        return this;
    }

    public String getPrenom() {
        return prenom;
    }

    public Employe setPrenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public String getNom() {
        return nom;
    }

    public Employe setNom(String nom) {
        this.nom = nom;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Employe setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Employe setPassword(String password) {
        this.password = password;
        return this;
    }

    public boolean isActif() {
        return actif;
    }

    public Employe setActif(boolean actif) {
        this.actif = actif;
        return this;
    }

    public Role getRole() {
        return role;
    }

    public Employe setRole(Role role) {
        this.role = role;
        return this;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    public Employe setDepartements(List<Departement> departements) {
        this.departements = departements;
        return this;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public Employe setContrat(Contrat contrat) {
        this.contrat = contrat;
        return this;
    }

    public List<Timesheet> getTimesheets() {
        return timesheets;
    }

    public Employe setTimesheets(List<Timesheet> timesheets) {
        this.timesheets = timesheets;
        return this;
    }

    @Override
    public String toString() {
        return "Employe [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", password="
                + password + ", actif=" + actif + ", role=" + role + "]";
    }
}
