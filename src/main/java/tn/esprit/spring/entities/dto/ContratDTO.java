package tn.esprit.spring.entities.dto;

import tn.esprit.spring.entities.Employe;

import java.util.Date;

public class ContratDTO {

    private int reference;

    private Date dateDebut;

    private String typeContrat;

    private Employe employe;

    private float salaire;

    public int getReference() {
        return reference;
    }

    public ContratDTO setReference(int reference) {
        this.reference = reference;
        return this;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public ContratDTO setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public String getTypeContrat() {
        return typeContrat;
    }

    public ContratDTO setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
        return this;
    }

    public Employe getEmploye() {
        return employe;
    }

    public ContratDTO setEmploye(Employe employe) {
        this.employe = employe;
        return this;
    }

    public float getSalaire() {
        return salaire;
    }

    public ContratDTO setSalaire(float salaire) {
        this.salaire = salaire;
        return this;
    }
}
