package tn.esprit.spring.entities.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.Timesheet;

import java.util.List;

public class EmployeDTO {

    private int id;

    private String prenom;

    private String nom;

    private String email;

    private String password;

    private boolean actif;

    private Role role;

    @JsonIgnore
    private List<Departement> departements;

    @JsonIgnore
    private Contrat contrat;

    @JsonIgnore
    private List<Timesheet> timesheets;

    public int getId() {
        return id;
    }

    public EmployeDTO setId(int id) {
        this.id = id;
        return this;
    }

    public String getPrenom() {
        return prenom;
    }

    public EmployeDTO setPrenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public String getNom() {
        return nom;
    }

    public EmployeDTO setNom(String nom) {
        this.nom = nom;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public EmployeDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public EmployeDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    public boolean isActif() {
        return actif;
    }

    public EmployeDTO setActif(boolean actif) {
        this.actif = actif;
        return this;
    }

    public Role getRole() {
        return role;
    }

    public EmployeDTO setRole(Role role) {
        this.role = role;
        return this;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    public EmployeDTO setDepartements(List<Departement> departements) {
        this.departements = departements;
        return this;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public EmployeDTO setContrat(Contrat contrat) {
        this.contrat = contrat;
        return this;
    }

    public List<Timesheet> getTimesheets() {
        return timesheets;
    }

    public EmployeDTO setTimesheets(List<Timesheet> timesheets) {
        this.timesheets = timesheets;
        return this;
    }
}
