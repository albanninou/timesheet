package tn.esprit.spring.entities.dto;

import tn.esprit.spring.entities.Departement;

import java.util.ArrayList;
import java.util.List;

public class EntrepriseDTO {

    private int id;

    private String name;

    private String raisonSocial;

    private List<Departement> departements = new ArrayList<>();

    public int getId() {
        return id;
    }

    public EntrepriseDTO setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public EntrepriseDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getRaisonSocial() {
        return raisonSocial;
    }

    public EntrepriseDTO setRaisonSocial(String raisonSocial) {
        this.raisonSocial = raisonSocial;
        return this;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    public EntrepriseDTO setDepartements(List<Departement> departements) {
        this.departements = departements;
        return this;
    }
}
