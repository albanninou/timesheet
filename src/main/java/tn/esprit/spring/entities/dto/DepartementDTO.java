package tn.esprit.spring.entities.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;

import java.util.List;

public class DepartementDTO {
    private int id;

    private String name;

    @JsonIgnore
    private List<Employe> employes;

    @JsonIgnore
    private List<Mission> missions;

    @JsonIgnore
    private Entreprise entreprise;

    public int getId() {
        return id;
    }

    public DepartementDTO setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public DepartementDTO setName(String name) {
        this.name = name;
        return this;
    }

    public List<Employe> getEmployes() {
        return employes;
    }

    public DepartementDTO setEmployes(List<Employe> employes) {
        this.employes = employes;
        return this;
    }

    public List<Mission> getMissions() {
        return missions;
    }

    public DepartementDTO setMissions(List<Mission> missions) {
        this.missions = missions;
        return this;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public DepartementDTO setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
        return this;
    }
}
