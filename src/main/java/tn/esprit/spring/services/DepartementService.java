package tn.esprit.spring.services;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;
import tn.esprit.spring.repository.MissionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DepartementService {

    final EntrepriseRepository entrepriseRepository;
    final DepartementRepository departementRepository;
    final MissionRepository missionRepository;
    private static final Logger log = Logger.getLogger(DepartementService.class);

    private static final String NOT_FOUND = " not found";

    public DepartementService(EntrepriseRepository entrepriseRepository, DepartementRepository departementRepository, MissionRepository missionRepository) {
        this.entrepriseRepository = entrepriseRepository;
        this.departementRepository = departementRepository;
        this.missionRepository = missionRepository;
    }

    private static void departementNotFount(int departementId) {
        log.error("Departement with id : " + departementId + NOT_FOUND);
    }

    @Transactional
    public Departement addOrUpdateDepartement(Departement departement) {
        departementRepository.save(departement);
        log.info("add or udate departement with id: " + departement.getId());
        return departement;
    }

    @Transactional
    public void affectDepartemenToEntreprise(int departementId, int entrepriseId) {
        Optional<Departement> departement = departementRepository.findById(departementId);
        Optional<Entreprise> entreprise = entrepriseRepository.findById(entrepriseId);

        if (!entreprise.isPresent()) {
            log.error("Entreprise with id : " + entrepriseId + NOT_FOUND);
            return;
        }

        if (!departement.isPresent()) {
            departementNotFount(departementId);
            return;
        }

        Departement departementManagedEntity = departement.get();
        Entreprise entrepriseManagedEntity = entreprise.get();

        if (entrepriseManagedEntity.getDepartements() == null) {
            List<Departement> departements = new ArrayList<>();
            departements.add(departementManagedEntity);
            entrepriseManagedEntity.setDepartements(departements);
        } else {
            entrepriseManagedEntity.getDepartements()
                                   .add(departementManagedEntity);
        }
        entrepriseRepository.save(entrepriseManagedEntity);
        log.info("affect departement with id: " + departementId + "to entreprise with id: " + entrepriseId);
    }

    @Transactional
    public void deleteDepartementOfEntreprise(int entrepriseId, int departementId) {
        Optional<Entreprise> entreprise = entrepriseRepository.findById(entrepriseId);

        if (!entreprise.isPresent()) {
            log.error("Entreprise with id : " + entrepriseId + NOT_FOUND);
            return;
        }
        Entreprise getEntreprise = entreprise.get();

        int departementNumber = getEntreprise.getDepartements()
                                             .size();
        for (int index = 0; index < departementNumber; index++) {
            if (getEntreprise.getDepartements()
                             .get(index)
                             .getId() == departementId) {
                getEntreprise.getDepartements()
                             .remove(index);
                break;
            }
        }
        log.info("delete departement with id: " + departementId + "to entreprise with id: " + entrepriseId);
    }

    @Transactional
    public String getDepartementNameById(int departementId) {
        Optional<Departement> departement = departementRepository.findById(departementId);

        if (!departement.isPresent()) {
            departementNotFount(departementId);
            return null;
        }
        return departement.get()
                          .getName();
    }

    public void deleteDepartementById(int departementId) {
        Optional<Departement> departement = departementRepository.findById(departementId);

        if (!departement.isPresent()) {
            departementNotFount(departementId);
            return;
        }
        Departement getDepartement = departement.get();

        Entreprise entreprise = getDepartement.getEntreprise();
        if (entreprise != null) entreprise.getDepartements()
                                          .remove(getDepartement);

        departementRepository.delete(getDepartement);
        log.info("delete departement with id: " + departementId);
    }

    public void updateNameByDepartementId(int departementId, String departementName) {
        Optional<Departement> departement = departementRepository.findById(departementId);
        if (!departement.isPresent()) {
            departementNotFount(departementId);
            return;
        }
        Departement dep = departement.get();
        dep.setName(departementName);
        departementRepository.save(dep);
        log.info("update name departement with id: " + departementId);
    }

    public void updateNameByDepartementIdJPQL(int departementId, String departementName) {
        Optional<Departement> departement = departementRepository.findById(departementId);

        if (!departement.isPresent()) {
            departementNotFount(departementId);
            return;
        }
        departement.get()
                   .setName(departementName);
        departementRepository.updateNameByDepartementIdJPQL(departementName, departementId);
        log.info("update name departement with id: " + departementId);
    }

    public long getDepartementNumberJPQL() {
        return departementRepository.count();
    }

    public List<Mission> getAllDepartementMissionsJPQL(int departementId) {
        return departementRepository.getAllDepartementMissions(departementId);
    }

    public List<Employe> getAllDepartementEmployesJPQL(int departementId) {
        return departementRepository.getAllDepartementEmployes(departementId);
    }

    public List<String> getAllDepartementsNameJPQL() {
        return departementRepository.departementsName();
    }

    public List<Departement> getAllDepartementByEntreprise(Entreprise entreprise) {
        return departementRepository.getAllDepartementByEntreprise(entreprise);
    }

    public List<Departement> getAllDepartements() {
        return (List<Departement>) departementRepository.findAll();
    }

}
