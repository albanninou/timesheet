package tn.esprit.spring.services;

import tn.esprit.spring.entities.Mission;

import java.util.List;

public interface IMissionService {

    int ajouterMission(Mission mission);

    List<String> getAllMissionsNamesByDepartement(int departementId);

    void affectMissionToDepartement(int departementId, int missionId);

    void deleteMissionById(int missionId);

    Mission getMissionById(int missionId);
}
