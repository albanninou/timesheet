package tn.esprit.spring.services;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.esprit.spring.entities.*;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.TimesheetRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeService {

    private static final Logger l = Logger.getLogger(EmployeService.class);
    final EmployeRepository employeRepository;
    final DepartementRepository deptRepository;
    final ContratRepository contractRepository;
    final TimesheetRepository timesheetRepository;

    public EmployeService(EmployeRepository employeRepository, DepartementRepository deptRepository, ContratRepository contractRepository, TimesheetRepository timesheetRepository) {
        this.employeRepository = employeRepository;
        this.deptRepository = deptRepository;
        this.contractRepository = contractRepository;
        this.timesheetRepository = timesheetRepository;
    }

    private static final String NOT_FOUND = " not found";

    private static void employNotFount(int employId) {
        l.error("Employ with id : " + employId + NOT_FOUND);
    }

    private static void departementNotFount(int depId) {
        l.error("Departement with id : " + depId + NOT_FOUND);
    }

    private static void contratNotFount(int contratId) {
        l.error("Contrat with id : " + contratId + NOT_FOUND);
    }


    public Employe authenticate(String login, String password) {
        return employeRepository.getEmployeByEmailAndPassword(login, password);
    }

    @Transactional
    public Employe addOrUpdateEmploye(Employe employe) {
        employeRepository.save(employe);
        return employe;
    }

    @Transactional
    public void mettreAjourEmailByEmployeId(String email, int employeId) {
        Optional<Employe> employe = employeRepository.findById(employeId);
        if (!employe.isPresent()) {
            employNotFount(employeId);
            return;
        }
        Employe e = employe.get();
        e.setEmail(email);
        employeRepository.save(e);
    }

    @Transactional
    public void affecterEmployeADepartement(int employeId, int depId) {
        Optional<Departement> departement = deptRepository.findById(depId);
        Optional<Employe> employe = employeRepository.findById(employeId);

        if (!departement.isPresent()) {
            departementNotFount(depId);
            return;
        }

        if (!employe.isPresent()) {
            employNotFount(employeId);
            return;
        }

        Departement depManagedEntity = departement.get();
        Employe employeManagedEntity = employe.get();

        if (depManagedEntity.getEmployes() == null) {
            List<Employe> employes = new ArrayList<>();
            employes.add(employeManagedEntity);
            depManagedEntity.setEmployes(employes);
        } else {
            depManagedEntity.getEmployes()
                            .add(employeManagedEntity);
        }
        deptRepository.save(depManagedEntity);
    }

    @Transactional
    public void desaffecterEmployeDuDepartement(int employeId, int depId) {
        Optional<Departement> departement = deptRepository.findById(depId);
        if (!departement.isPresent()) {
            departementNotFount(depId);
            return;
        }
        Departement dep = departement.get();

        int employeNb = dep.getEmployes()
                           .size();
        for (int index = 0; index < employeNb; index++) {
            if (dep.getEmployes()
                   .get(index)
                   .getId() == employeId) {
                dep.getEmployes()
                   .remove(index);
                break;//a revoir
            }
        }
    }

    public int ajouterContrat(Contrat contrat) {
        contractRepository.save(contrat);
        return contrat.getReference();
    }

    public void affecterContratAEmploye(int contratId, int employeId) {
        Optional<Contrat> contrat = contractRepository.findById(contratId);
        Optional<Employe> employe = employeRepository.findById(employeId);

        if (!employe.isPresent()) {
            employNotFount(employeId);
            return;
        }
        if (!contrat.isPresent()) {
            contratNotFount(contratId);
            return;
        }

        Contrat contratManagedEntity = contrat.get();
        Employe employeManagedEntity = employe.get();

        contratManagedEntity.setEmploye(employeManagedEntity);
        contractRepository.save(contratManagedEntity);

    }

    public String getEmployePrenomById(int employeId) {
        Optional<Employe> employe = employeRepository.findById(employeId);
        if (!employe.isPresent()) {
            employNotFount(employeId);
            return null;
        }
        return employe.get()
                      .getPrenom();
    }

    public void deleteEmployeById(int employeId) {
        Optional<Employe> e = employeRepository.findById(employeId);

        if (!e.isPresent()) {
            employNotFount(employeId);
            return;
        }
        Employe employe = e.get();

        //Desaffecter l'employe de tous les departements
        //c'est le bout master qui permet de mettre a jour
        //la table d'association
        for (Departement dep : employe.getDepartements()) {
            dep.getEmployes()
               .remove(employe);
        }

        employeRepository.delete(employe);
    }

    public void deleteContratById(int contratId) {
        Optional<Contrat> contratManagedEntity = contractRepository.findById(contratId);
        if (!contratManagedEntity.isPresent()) {
            contratNotFount(contratId);
            return;
        }
        contractRepository.delete(contratManagedEntity.get());
    }

    public long getNombreEmployeJPQL() {
        return employeRepository.count();
    }

    public List<String> getAllEmployeNamesJPQL() {
        return employeRepository.employeNames();

    }

    public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
        return employeRepository.getAllEmployeByEntreprisec(entreprise);
    }

    public void mettreAjourEmailByEmployeIdJPQL(String email, int employeId) {
        employeRepository.mettreAjourEmailByEmployeIdJPQL(email, employeId);

    }

    public void deleteAllContratJPQL() {
        employeRepository.deleteAllContratJPQL();
    }

    public float getSalaireByEmployeIdJPQL(int employeId) {
        return employeRepository.getSalaireByEmployeIdJPQL(employeId);
    }

    public Double getSalaireMoyenByDepartementId(int departementId) {
        return employeRepository.getSalaireMoyenByDepartementId(departementId);
    }

    public List<Timesheet> getTimesheetsByMissionAndDate(Employe employe, Mission mission, Date dateDebut,
                                                         Date dateFin) {
        return timesheetRepository.getTimesheetsByMissionAndDate(employe, mission, dateDebut, dateFin);
    }

    public List<Employe> getAllEmployes() {
        return (List<Employe>) employeRepository.findAll();
    }
}
