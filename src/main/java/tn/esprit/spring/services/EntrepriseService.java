package tn.esprit.spring.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EntrepriseService {

    private static final Logger l = Logger.getLogger(EntrepriseService.class);

    @Autowired
    EntrepriseRepository entrepriseRepoistory;
    @Autowired
    DepartementRepository deptRepoistory;

    public int ajouterEntreprise(Entreprise entreprise) {
        entrepriseRepoistory.save(entreprise);
        return entreprise.getId();
    }

    public int ajouterDepartement(Departement dep) {
        deptRepoistory.save(dep);
        return dep.getId();
    }

    //Permet d'afficher les erreurs
    private static void entrepriseNotFount(int entrepriseId) {
        l.error("business with id : " + entrepriseId + " not found");
    }

    //Permet d'afficher les erreurs
    private static void departementNotFount(int depId) {
        l.error("Department with id : " + depId + " not found");
    }

    public void affecterDepartementAEntreprise(int depId, int entrepriseId) {

        Optional<Entreprise> entreprise = entrepriseRepoistory.findById(entrepriseId);
        Optional<Departement> departement = deptRepoistory.findById(depId);

        if (!entreprise.isPresent()) {
            entrepriseNotFount(entrepriseId);
            return;
        }

        if (!departement.isPresent()) {
            departementNotFount(depId);
            return;
        }
        Entreprise e = entreprise.get();
        Departement d = departement.get();

        d.setEntreprise(e);
        deptRepoistory.save(d);
    }

    public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {

        Optional<Entreprise> entreprise = entrepriseRepoistory.findById(entrepriseId);
        if (!entreprise.isPresent()) {
            entrepriseNotFount(entrepriseId);
            return new ArrayList<>();
        }
        Entreprise e = entreprise.get();

        List<String> depNames = new ArrayList<>();
        for (Departement dep : e.getDepartements()) {
            depNames.add(dep.getName());
        }

        return depNames;
    }

    @Transactional
    public void deleteEntrepriseById(int entrepriseId) {

        Optional<Entreprise> e = entrepriseRepoistory.findById(entrepriseId);

        if (!e.isPresent()) {
            entrepriseNotFount(entrepriseId);
            return;
        }
        Entreprise entreprise = e.get();
        entrepriseRepoistory.delete(entreprise);
    }

    @Transactional
    public void deleteDepartementById(int depId) {
        Optional<Departement> departementManagedEntity = deptRepoistory.findById(depId);
        if (!departementManagedEntity.isPresent()) {
            l.error("Departement with id : " + depId + " not identified");
            return;
        }
        deptRepoistory.delete(departementManagedEntity.get());
    }


    public Entreprise getEntrepriseById(int entrepriseId) {

        Optional<Entreprise> entreprise = entrepriseRepoistory.findById(entrepriseId);
        if (!entreprise.isPresent()) {
            entrepriseNotFount(entrepriseId);
            return null;
        }
        return entreprise.get();
    }
}
