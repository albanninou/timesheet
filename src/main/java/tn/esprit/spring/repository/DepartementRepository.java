package tn.esprit.spring.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;

import java.util.List;

@Repository
public interface DepartementRepository extends CrudRepository<Departement, Integer> {

    @Query("SELECT d FROM Departement d WHERE d.name=:name")
    Departement getDepartementByHisName(@Param("name") String name);

    @Query("SELECT name FROM Departement")
    List<String> departementsName();

    @Query("Select "
            + "DISTINCT d from Departement d "
            + "join d.entreprise entrep "
            + "where entrep=:entreprise")
    List<Departement> getAllDepartementByEntreprise(@Param("entreprise") Entreprise entreprise);

    @Query("Select "
            + "DISTINCT e from Employe e "
            + "join e.departements d "
            + "where d.id=:departementId")
    List<Employe> getAllDepartementEmployes(@Param("departementId") int departementId);

    @Query("Select "
            + "DISTINCT m from Mission m "
            + "join m.departement d "
            + "where d.id=:departementId")
    List<Mission> getAllDepartementMissions(@Param("departementId") int departementId);

    @Modifying
    @Transactional
    @Query("UPDATE Departement d SET d.name=:name where d.id=:departementId")
    void updateNameByDepartementIdJPQL(@Param("name") String name, @Param("departementId") int departementId);

}
