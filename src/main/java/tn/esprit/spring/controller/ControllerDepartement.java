package tn.esprit.spring.controller;

import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.services.DepartementService;

import java.util.List;

@Scope(value = "session")
@Controller(value = "departementController")
@ELBeanName(value = "departementController")
public class ControllerDepartement {

    final DepartementService departementService;

    private String name;
    private List<Mission> missions;
    private Entreprise entreprise;
    private List<Employe> employes;
    private Integer id;

    public ControllerDepartement(DepartementService departementService) {
        this.departementService = departementService;
    }

    public String addThisDepartement() {
        departementService.addOrUpdateDepartement(new Departement(name));
        return "null";
    }

    public String removeThisDepartement(int departementId) {
        departementService.deleteDepartementById(departementId);
        return "null";
    }

    public String displayThisDepartement(Departement departement) {
        this.setName(departement.getName());
        this.setDepartementIdToBeUpdated(departement.getId());
        return "null";
    }

    public String updateThisDepartement() {
        departementService.addOrUpdateDepartement(new Departement(id, name));
        return "null";
    }

    public List<Departement> getAllDepartements() {
        return departementService.getAllDepartements();
    }

    public String addDepartement(Departement departement) {
        departementService.addOrUpdateDepartement(departement);
        return "null";
    }

    public String updateDepartement(String name) {
        departementService.updateNameByDepartementIdJPQL(id, name);
        return "null";
    }

    public String affectDepartementToEntreprise(int departementId, int entrepriseId) {
        departementService.affectDepartemenToEntreprise(departementId, entrepriseId);
        return "null";
    }

    public String deleteDepartementToEntreprise(int departementId, int entrepriseId) {
        departementService.deleteDepartementOfEntreprise(departementId, entrepriseId);
        return "null";
    }

    public String getDepartementNameById(int departementId) {
        return departementService.getDepartementNameById(departementId);
    }

    public void deleteDepartementById(int departementId) {
        departementService.deleteDepartementById(departementId);
    }

    public long getNumberDepartement() {
        return departementService.getDepartementNumberJPQL();
    }

    public List<String> getNamesDepartement() {
        return departementService.getAllDepartementsNameJPQL();
    }

    public List<Departement> getAllDepartementsByEntreprise(Entreprise entreprise) {
        return departementService.getAllDepartementByEntreprise(entreprise);
    }

    public List<Employe> getAllEmployesByDepartement(int departementId) {
        return departementService.getAllDepartementEmployesJPQL(departementId);
    }

    public List<Mission> getAllMissionsByDepartement(int departementId) {
        return departementService.getAllDepartementMissionsJPQL(departementId);
    }

    public Integer getDepartementId() {
        return id;
    }

    public void setDepartementIdToBeUpdated(Integer departementId) {
        this.id = departementId;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public List<Employe> getEmployes() {
        return employes;
    }


    public void setEmployes(List<Employe> employes) {
        this.employes = employes;
    }


    public List<Mission> getMissions() {
        return missions;
    }


    public void setMissions(List<Mission> missions) {
        this.missions = missions;
    }


    public Entreprise getEntreprise() {
        return entreprise;
    }


    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
}
