package tn.esprit.spring.controller;

import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.dto.DepartementDTO;
import tn.esprit.spring.mapper.DepartementMapper;
import tn.esprit.spring.services.DepartementService;
import tn.esprit.spring.services.EntrepriseService;
import tn.esprit.spring.services.IMissionService;

import java.util.List;

public class RestControlDepartement {

    final EntrepriseService entrepriseService;
    final DepartementService departementService;
    final IMissionService missionService;

    public RestControlDepartement(EntrepriseService entrepriseService, DepartementService departementService, IMissionService missionService) {
        this.entrepriseService = entrepriseService;
        this.departementService = departementService;
        this.missionService = missionService;
    }

    @PostMapping("/addDepartement")
    @ResponseBody
    public Departement addDepartement(@RequestBody DepartementDTO departementDTO) {
        return departementService.addOrUpdateDepartement(DepartementMapper.toEntity(departementDTO));
    }

    @PutMapping(value = "/modifyName/{id}/{newName}")
    @ResponseBody
    public void mettreAjourEmailByDepartementId(@PathVariable("newName") String name, @PathVariable("id") int departementId) {
        departementService.updateNameByDepartementIdJPQL(departementId, name);

    }

    @PutMapping(value = "/affectDepartementToEntreprise/{departementId}/{entrepriseId}")
    public void affecterDepartementADepartement(@PathVariable("entrepriseId") int entrepriseId, @PathVariable("departementId") int departementId) {
        departementService.affectDepartemenToEntreprise(departementId, entrepriseId);

    }

    @PutMapping(value = "/deleteDeprtementToEntreprise/{departementId}/{entrepriseId}")
    public void desaffecterDepartementDuDepartement(@PathVariable("entrepriseId") int entrepriseId, @PathVariable("departementId") int departementId) {
        departementService.deleteDepartementOfEntreprise(entrepriseId, departementId);
    }

    @GetMapping(value = "getDepartementNameById/{departementId}")
    @ResponseBody
    public String getDepartementPrenomById(@PathVariable("departementId") int departementId) {
        return departementService.getDepartementNameById(departementId);
    }

    @DeleteMapping("/deleteDepartementById/{departementId}")
    @ResponseBody
    public void deleteDepartementById(@PathVariable("departementId") int departementId) {
        departementService.deleteDepartementById(departementId);
    }

    @GetMapping(value = "getNumberOfDepartements")
    @ResponseBody
    public long getNumberOfDepartements() {
        return departementService.getDepartementNumberJPQL();
    }

    @GetMapping(value = "getAllDepartementNames")
    @ResponseBody
    public List<String> getAllDepartementNames() {
        return departementService.getAllDepartementsNameJPQL();
    }

    @GetMapping(value = "getAllDepartementByEntreprise/{entrepriseId}")
    @ResponseBody
    public List<Departement> getAllDepartementByEntreprise(@PathVariable("entrepriseId") int entrepriseId) {
        Entreprise entreprise = entrepriseService.getEntrepriseById(entrepriseId);
        return departementService.getAllDepartementByEntreprise(entreprise);
    }

    @GetMapping(value = "/getAllDepartements")
    @ResponseBody
    public List<Departement> getAllDepartements() {
        return departementService.getAllDepartements();
    }

    @GetMapping(value = "/getAllMissionsByDepartement/{departementId}")
    @ResponseBody
    public List<Mission> getAllMissionsByDepartement(@PathVariable("departementId") int departementId) {
        return departementService.getAllDepartementMissionsJPQL(departementId);
    }

    @GetMapping(value = "/getAllEmployesByDepartement/{departementId}")
    @ResponseBody
    public List<Employe> getAllEmployesByDepartement(@PathVariable("departementId") int departementId) {
        return departementService.getAllDepartementEmployesJPQL(departementId);
    }
}
