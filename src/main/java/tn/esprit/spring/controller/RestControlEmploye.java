package tn.esprit.spring.controller;

import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.entities.*;
import tn.esprit.spring.entities.dto.ContratDTO;
import tn.esprit.spring.entities.dto.EmployeDTO;
import tn.esprit.spring.mapper.ContratMapper;
import tn.esprit.spring.mapper.EmployeMapper;
import tn.esprit.spring.services.EmployeService;
import tn.esprit.spring.services.EntrepriseService;
import tn.esprit.spring.services.ITimesheetService;

import java.util.Date;
import java.util.List;

@RestController
public class RestControlEmploye {

    final EmployeService employeService;
    final EntrepriseService entrepriseService;
    final ITimesheetService itimesheetservice;

    public RestControlEmploye(EmployeService employeService, EntrepriseService entrepriseService, ITimesheetService itimesheetservice) {
        this.employeService = employeService;
        this.entrepriseService = entrepriseService;
        this.itimesheetservice = itimesheetservice;
    }

    @PostMapping("/ajouterEmployer")
    @ResponseBody
    public Employe ajouterEmploye(@RequestBody EmployeDTO employeDTO) {
        return employeService.addOrUpdateEmploye(EmployeMapper.toEntity(employeDTO));
    }

    // Modifier email : http://localhost:8081/SpringMVC/servlet/modifyEmail/1/newemail
    @PutMapping(value = "/modifyEmail/{id}/{newemail}")
    @ResponseBody
    public void mettreAjourEmailByEmployeId(@PathVariable("newemail") String email, @PathVariable("id") int employeId) {
        employeService.mettreAjourEmailByEmployeId(email, employeId);

    }

    // http://localhost:8081/SpringMVC/servlet/affecterEmployeADepartement/1/1
    @PutMapping(value = "/affecterEmployeADepartement/{idemp}/{iddept}")
    public void affecterEmployeADepartement(@PathVariable("idemp") int employeId, @PathVariable("iddept") int depId) {
        employeService.affecterEmployeADepartement(employeId, depId);

    }

    // http://localhost:8081/SpringMVC/servlet/desaffecterEmployeDuDepartement/1/1
    @PutMapping(value = "/desaffecterEmployeDuDepartement/{idemp}/{iddept}")
    public void desaffecterEmployeDuDepartement(@PathVariable("idemp") int employeId, @PathVariable("iddept") int depId) {
        employeService.desaffecterEmployeDuDepartement(employeId, depId);
    }

    // http://localhost:8081/SpringMVC/servlet/ajouterContrat
    @PostMapping("/ajouterContrat")
    @ResponseBody
    public int ajouterContrat(@RequestBody ContratDTO contratDTO) {
        Contrat contrat = ContratMapper.toEntity(contratDTO);
        employeService.ajouterContrat(contrat);
        return contrat.getReference();
    }

    // http://localhost:8081/SpringMVC/servlet/affecterContratAEmploye/6/1
    @PutMapping(value = "/affecterContratAEmploye/{idcontrat}/{idemp}")
    public void affecterContratAEmploye(@PathVariable("idcontrat") int contratId, @PathVariable("idemp") int employeId) {
        employeService.affecterContratAEmploye(contratId, employeId);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/getEmployePrenomById/2
    @GetMapping(value = "getEmployePrenomById/{idemp}")
    @ResponseBody
    public String getEmployePrenomById(@PathVariable("idemp") int employeId) {
        return employeService.getEmployePrenomById(employeId);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/deleteEmployeById/1
    @DeleteMapping("/deleteEmployeById/{idemp}")
    @ResponseBody
    public void deleteEmployeById(@PathVariable("idemp") int employeId) {
        employeService.deleteEmployeById(employeId);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/deleteContratById/2
    @DeleteMapping("/deleteContratById/{idcontrat}")
    @ResponseBody
    public void deleteContratById(@PathVariable("idcontrat") int contratId) {
        employeService.deleteContratById(contratId);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/getNombreEmployeJPQL
    @GetMapping(value = "getNombreEmployeJPQL")
    @ResponseBody
    public long getNombreEmployeJPQL() {
        return employeService.getNombreEmployeJPQL();
    }

    // URL : http://localhost:8081/SpringMVC/servlet/getAllEmployeNamesJPQL
    @GetMapping(value = "getAllEmployeNamesJPQL")
    @ResponseBody
    public List<String> getAllEmployeNamesJPQL() {
        return employeService.getAllEmployeNamesJPQL();
    }

    // URL : http://localhost:8081/SpringMVC/servlet/getAllEmployeByEntreprise/1
    @GetMapping(value = "getAllEmployeByEntreprise/{identreprise}")
    @ResponseBody
    public List<Employe> getAllEmployeByEntreprise(@PathVariable("identreprise") int identreprise) {
        Entreprise entreprise = entrepriseService.getEntrepriseById(identreprise);
        return employeService.getAllEmployeByEntreprise(entreprise);
    }

    // Modifier email : http://localhost:8081/SpringMVC/servlet/mettreAjourEmailByEmployeIdJPQL/2/newemail
    @PutMapping(value = "/mettreAjourEmailByEmployeIdJPQL/{id}/{newemail}")
    @ResponseBody
    public void mettreAjourEmailByEmployeIdJPQL(@PathVariable("newemail") String email, @PathVariable("id") int employeId) {
        employeService.mettreAjourEmailByEmployeIdJPQL(email, employeId);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/deleteAllContratJPQL
    @DeleteMapping("/deleteAllContratJPQL")
    @ResponseBody
    public void deleteAllContratJPQL() {
        employeService.deleteAllContratJPQL();
    }

    // URL : http://localhost:8081/SpringMVC/servlet/getSalaireByEmployeIdJPQL/2
    @GetMapping(value = "getSalaireByEmployeIdJPQL/{idemp}")
    @ResponseBody
    public float getSalaireByEmployeIdJPQL(@PathVariable("idemp") int employeId) {
        return employeService.getSalaireByEmployeIdJPQL(employeId);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/getSalaireMoyenByDepartementId/2
    @GetMapping(value = "getSalaireMoyenByDepartementId/{iddept}")
    @ResponseBody
    public Double getSalaireMoyenByDepartementId(@PathVariable("iddept") int departementId) {
        return employeService.getSalaireMoyenByDepartementId(departementId);
    }

    public List<Timesheet> getTimesheetsByMissionAndDate(Employe employe, Mission mission, Date dateDebut,
                                                         Date dateFin) {
        return employeService.getTimesheetsByMissionAndDate(employe, mission, dateDebut, dateFin);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/getAllEmployes
    @GetMapping(value = "/getAllEmployes")
    @ResponseBody
    public List<Employe> getAllEmployes() {
        return employeService.getAllEmployes();
    }
}
