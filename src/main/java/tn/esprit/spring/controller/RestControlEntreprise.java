package tn.esprit.spring.controller;

import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.dto.DepartementDTO;
import tn.esprit.spring.entities.dto.EntrepriseDTO;
import tn.esprit.spring.mapper.DepartementMapper;
import tn.esprit.spring.mapper.EntrepriseMapper;
import tn.esprit.spring.services.EmployeService;
import tn.esprit.spring.services.EntrepriseService;
import tn.esprit.spring.services.ITimesheetService;

import java.util.List;

@RestController
public class RestControlEntreprise {

    final EmployeService employeService;
    final EntrepriseService entrepriseService;
    final ITimesheetService itimesheetservice;

    public RestControlEntreprise(EmployeService employeService, EntrepriseService entrepriseService, ITimesheetService itimesheetservice) {
        this.employeService = employeService;
        this.entrepriseService = entrepriseService;
        this.itimesheetservice = itimesheetservice;
    }

    // Ajouter Entreprise : http://localhost:8081/SpringMVC/servlet/ajouterEntreprise

    @PostMapping("/ajouterEntreprise")
    @ResponseBody
    public int ajouterEntreprise(@RequestBody EntrepriseDTO ssiiConsulting) {
        entrepriseService.ajouterEntreprise(EntrepriseMapper.toEntity(ssiiConsulting));
        return ssiiConsulting.getId();
    }

    // http://localhost:8081/SpringMVC/servlet/affecterDepartementAEntreprise/1/1
    @PutMapping(value = "/affecterDepartementAEntreprise/{iddept}/{identreprise}")
    public void affecterDepartementAEntreprise(@PathVariable("iddept") int depId, @PathVariable("identreprise") int entrepriseId) {
        entrepriseService.affecterDepartementAEntreprise(depId, entrepriseId);
    }

    // http://localhost:8081/SpringMVC/servlet/deleteEntrepriseById/1
    @DeleteMapping("/deleteEntrepriseById/{identreprise}")
    @ResponseBody
    public void deleteEntrepriseById(@PathVariable("identreprise") int entrepriseId) {
        entrepriseService.deleteEntrepriseById(entrepriseId);
    }

    // http://localhost:8081/SpringMVC/servlet/getEntrepriseById/1
    @GetMapping(value = "getEntrepriseById/{identreprise}")
    @ResponseBody
    public Entreprise getEntrepriseById(@PathVariable("identreprise") int entrepriseId) {

        return entrepriseService.getEntrepriseById(entrepriseId);
    }

    // http://localhost:8081/SpringMVC/servlet/ajouterDepartement

    @PostMapping("/ajouterDepartement")
    @ResponseBody
    public int ajouterDepartement(@RequestBody DepartementDTO departementDTO) {
        return entrepriseService.ajouterDepartement(DepartementMapper.toEntity(departementDTO));
    }

    // http://localhost:8081/SpringMVC/servlet/getAllDepartementsNamesByEntreprise/1
    @GetMapping(value = "getAllDepartementsNamesByEntreprise/{identreprise}")
    @ResponseBody
    public List<String> getAllDepartementsNamesByEntreprise(@PathVariable("identreprise") int entrepriseId) {
        return entrepriseService.getAllDepartementsNamesByEntreprise(entrepriseId);
    }

    // URL : http://localhost:8081/SpringMVC/servlet/deleteDepartementById/3
    @DeleteMapping("/deleteDepartementById/{iddept}")
    @ResponseBody
    public void deleteDepartementById(@PathVariable("iddept") int depId) {
        entrepriseService.deleteDepartementById(depId);

    }
}
