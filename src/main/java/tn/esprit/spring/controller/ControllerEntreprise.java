package tn.esprit.spring.controller;

import org.springframework.stereotype.Controller;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.services.EmployeService;
import tn.esprit.spring.services.EntrepriseService;
import tn.esprit.spring.services.ITimesheetService;

import java.util.List;

@Controller
public class ControllerEntreprise {

    final EmployeService employeService;
    final EntrepriseService entrepriseService;
    final ITimesheetService itimesheetservice;

    public ControllerEntreprise(EmployeService employeService, EntrepriseService entrepriseService, ITimesheetService itimesheetservice) {
        this.employeService = employeService;
        this.entrepriseService = entrepriseService;
        this.itimesheetservice = itimesheetservice;
    }

    public int ajouterEntreprise(Entreprise ssiiConsulting) {
        entrepriseService.ajouterEntreprise(ssiiConsulting);
        return ssiiConsulting.getId();
    }

    public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
        entrepriseService.affecterDepartementAEntreprise(depId, entrepriseId);
    }

    public void deleteEntrepriseById(int entrepriseId) {
        entrepriseService.deleteEntrepriseById(entrepriseId);
    }

    public Entreprise getEntrepriseById(int entrepriseId) {

        return entrepriseService.getEntrepriseById(entrepriseId);
    }

    public int ajouterDepartement(Departement dep) {
        return entrepriseService.ajouterDepartement(dep);
    }

    public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
        return entrepriseService.getAllDepartementsNamesByEntreprise(entrepriseId);
    }

    public void deleteDepartementById(int depId) {
        entrepriseService.deleteDepartementById(depId);

    }
}
