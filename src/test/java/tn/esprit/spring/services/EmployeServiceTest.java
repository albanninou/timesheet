package tn.esprit.spring.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.repository.EmployeRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EmployeServiceTest {

    @Autowired
    private EmployeService employeService;

    @Autowired
    private EmployeRepository employeRepository;

    @BeforeEach
    void setup() {
        employeRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        employeRepository.deleteAll();
    }

    @Test
    void addOrUpdateEmployeTest() {
        assertEquals(0, employeRepository.count());
        Employe employe = new Employe().setPrenom("prenom");
        employeService.addOrUpdateEmploye(employe);
        assertEquals(1, employeRepository.count());

        Employe entity = employeRepository.findAll()
                                          .iterator()
                                          .next();
        assertEquals("prenom", entity.getPrenom());
        employeService.addOrUpdateEmploye(employe.setPrenom("update"));
        assertEquals(1, employeRepository.count());
        entity = employeRepository.findAll()
                                  .iterator()
                                  .next();
        assertEquals("update", entity.getPrenom());
    }

    @Test
    void mettreAjourEmailByEmployeIdTest() {
        employeRepository.save(new Employe().setId(1)
                                            .setEmail("email"));
        employeService.mettreAjourEmailByEmployeId("update", 1);
        Employe entity = employeRepository.findAll()
                                          .iterator()
                                          .next();
        assertEquals("update", entity.getEmail());
    }
}
