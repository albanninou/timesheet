package tn.esprit.spring.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.EntrepriseRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EntrepriseServiceTest {

    @Autowired
    private EntrepriseService entrepriseService;

    @Autowired
    private EntrepriseRepository entrepriseRepository;

    @BeforeEach
    void setup() {
        entrepriseRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        entrepriseRepository.deleteAll();
    }

    @Test
    void addOrUpdateEntrepriseTest() {
        assertEquals(0, entrepriseRepository.count());
        Entreprise entreprise = new Entreprise().setName("name");
        entrepriseService.ajouterEntreprise(entreprise);
        assertEquals(1, entrepriseRepository.count());

        Entreprise entity = entrepriseRepository.findAll()
                                                .iterator()
                                                .next();
        assertEquals("name", entity.getName());
    }
}
