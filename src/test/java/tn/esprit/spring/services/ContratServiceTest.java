package tn.esprit.spring.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.repository.ContratRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ContratServiceTest {

    @Autowired
    private ContratService contratService;

    @Autowired
    private ContratRepository contratRepository;

    @BeforeEach
    void setup() {
        contratRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        contratRepository.deleteAll();
    }

    @Test
    void getAllContratsTest() {
        assertEquals(0, contratRepository.count());
        contratRepository.save(new Contrat().setTypeContrat("contrat")
                                            .setSalaire(0.2f));
        contratRepository.save(new Contrat().setTypeContrat("contrat")
                                            .setSalaire(0.2f));
        List<Contrat> allContrats = contratService.getAllContrats();
        assertEquals(2, allContrats.size());
    }

}
