package tn.esprit.spring.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.repository.DepartementRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DepartementServiceTest {

    @Autowired
    private DepartementService departementService;

    @Autowired
    private DepartementRepository departementRepository;

    @BeforeEach
    void setup() {
        departementRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        departementRepository.deleteAll();
    }

    @Test
    void addOrUpdateDepartementTest() {
        assertEquals(0, departementRepository.count());
        Departement departement = new Departement().setName("depName");
        departementService.addOrUpdateDepartement(departement);
        assertEquals(1, departementRepository.count());

        Departement entity = departementRepository.findAll()
                                                  .iterator()
                                                  .next();
        assertEquals("depName", entity.getName());
        departementService.addOrUpdateDepartement(departement.setName("update"));
        assertEquals(1, departementRepository.count());

        entity = departementRepository.findAll()
                                      .iterator()
                                      .next();
        assertEquals("update", entity.getName());
    }

    @Test
    void updateNameByDepartementIdTest() {
        departementService.addOrUpdateDepartement(new Departement().setName("name"));
        departementService.updateNameByDepartementId(departementRepository.getDepartementByHisName("name")
                                                                          .getId(), "update");
        Departement entity = departementRepository.findAll()
                                                  .iterator()
                                                  .next();
        assertEquals("update", entity.getName());
    }

    @Test
    void deleteDepartementByIdTest() {
        departementService.addOrUpdateDepartement(new Departement("name"));
        departementService.deleteDepartementById(departementRepository.getDepartementByHisName("name")
                                                                      .getId());
        assertEquals(0, departementRepository.count());
    }
}
