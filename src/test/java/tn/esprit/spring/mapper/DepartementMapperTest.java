package tn.esprit.spring.mapper;

import org.junit.jupiter.api.Test;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.dto.DepartementDTO;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DepartementMapperTest {

    @Test
    void toEntityTest() {
        DepartementDTO departementDTO = new DepartementDTO().setId(1)
                                                            .setName("name")
                                                            .setEmployes(new ArrayList<Employe>())
                                                            .setMissions(new ArrayList<Mission>())
                                                            .setEntreprise(new Entreprise());

        Departement departement = DepartementMapper.toEntity(departementDTO);
        assertEquals(departementDTO.getId(), departement.getId());
        assertEquals(departementDTO.getName(), departement.getName());
        assertEquals(departementDTO.getEmployes(), departement.getEmployes());
        assertEquals(departementDTO.getMissions(), departement.getMissions());
        assertEquals(departementDTO.getEntreprise(), departement.getEntreprise());
    }

}
