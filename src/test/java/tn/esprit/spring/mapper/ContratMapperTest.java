package tn.esprit.spring.mapper;

import org.junit.jupiter.api.Test;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.dto.ContratDTO;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ContratMapperTest {

    @Test
    void toEntityTest() {
        ContratDTO contratDTO = new ContratDTO().setTypeContrat("contrat")
                                                .setDateDebut(new Date())
                                                .setEmploye(new Employe())
                                                .setReference(50)
                                                .setSalaire(20.5f);

        Contrat contrat = ContratMapper.toEntity(contratDTO);
        assertEquals(contratDTO.getTypeContrat(), contrat.getTypeContrat());
        assertEquals(contratDTO.getEmploye(), contrat.getEmploye());
        assertEquals(contratDTO.getReference(), contrat.getReference());
        assertEquals(contratDTO.getSalaire(), contrat.getSalaire());
        assertEquals(contratDTO.getDateDebut(), contrat.getDateDebut());
    }

}
