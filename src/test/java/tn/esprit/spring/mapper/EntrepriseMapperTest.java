package tn.esprit.spring.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.dto.EntrepriseDTO;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EntrepriseMapperTest {

    @Test
    void toEntityTest() {
        EntrepriseDTO entrepriseDTO = new EntrepriseDTO().setId(123)
                                                         .setName("Name")
                                                         .setRaisonSocial("prenom")
                                                         .setDepartements(new ArrayList<>());

        Entreprise entreprise = EntrepriseMapper.toEntity(entrepriseDTO);
        assertEquals(entrepriseDTO.getId(), entreprise.getId());
        assertEquals(entrepriseDTO.getName(), entreprise.getName());
        assertEquals(entrepriseDTO.getRaisonSocial(), entreprise.getRaisonSocial());
        assertEquals(entrepriseDTO.getDepartements(), entreprise.getDepartements());
    }
}
