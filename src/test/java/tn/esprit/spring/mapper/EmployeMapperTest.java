package tn.esprit.spring.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.dto.EmployeDTO;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EmployeMapperTest {

    @Test
    void toEntityTest() {
        EmployeDTO employeDTO = new EmployeDTO().setId(123)
                                                .setEmail("email")
                                                .setPrenom("prenom")
                                                .setNom("nom")
                                                .setActif(true)
                                                .setContrat(new Contrat())
                                                .setDepartements(new ArrayList<>())
                                                .setPassword("password")
                                                .setTimesheets(new ArrayList<>())
                                                .setRole(Role.ADMINISTRATEUR);

        Employe employe = EmployeMapper.toEntity(employeDTO);
        assertEquals(employeDTO.getId(), employe.getId());
        assertEquals(employeDTO.getEmail(), employe.getEmail());
        assertEquals(employeDTO.getPassword(), employe.getPassword());
        assertEquals(employeDTO.getPrenom(), employe.getPrenom());
        assertEquals(employeDTO.getNom(), employe.getNom());
        assertEquals(employeDTO.isActif(), employe.isActif());
        assertEquals(employeDTO.getContrat(), employe.getContrat());
        assertEquals(employeDTO.getDepartements(), employe.getDepartements());
        assertEquals(employeDTO.getTimesheets(), employe.getTimesheets());
        assertEquals(employeDTO.getRole(), employe.getRole());
    }
}
